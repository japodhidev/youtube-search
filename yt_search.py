import re
import os
import json

import google_auth_oauthlib.flow
import googleapiclient.discovery
import googleapiclient.errors


def extractNames():
    ''' Extract song names from a text file containing song names
        e.g 'Song Name - Favorite Artist.mp3'. The text file was
        a dump from an AIMP playlist (heads up).
    '''
    regx_comp = r"(\w+\W*\s)*(\w+)(\s\-\s)(\w+\W*\w\s)*(\D\w+\D*)(.[a-zA-Z]{2}[3])"

    with open("playlist-snippet.txt", "r") as lines:
        names = []
        for l in lines:
            song = re.search(regx_comp, l)
            if song is not None:
                # print(song.group())
                names.append(song.group())
    print("%d long." % len(names))

    with open("songs.txt", "w") as songs:
        for n in names:
            n = n.replace('.mp3', '')
            songs.write("%s\n" % n)


def search():
    ''' Google Youtube Data API search. Find the most relevant result and
    dump the result to a json file. '''

    # TODO: Optionally add an input file as a parameter
    scopes = ["https://www.googleapis.com/auth/youtube.readonly"]
    # Disable OAuthlib's HTTPS verification when running locally.
    os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = "1"

    api_service_name = "youtube"
    api_version = "v3"
    client_secrets_file = "secrets.json"

    # Get credentials and create an API client
    flow = google_auth_oauthlib.flow.InstalledAppFlow.from_client_secrets_file(
        client_secrets_file, scopes)
    credentials = flow.run_console()
    youtube = googleapiclient.discovery.build(
        api_service_name, api_version, credentials=credentials)

    # TODO: Wrap file opening in a try..catch block
    full = []
    with open("hiphop.txt", "r") as tests:
        print("Authentication done. Writing search results to file.")
        for t in tests:
            # Return only the first and most relevant result.
            # To save on response.json's size
            request = youtube.search().list(
                part="snippet",
                maxResults=1,
                q=t,
                prettyPrint=True
            )
            response = request.execute()
            full.append(response)
    with open("response.json", "w") as res:
        res.write(json.dumps(full))


def parseJson():
    ''' Open response.json and extract id values (videoId).
        Append id to Youtube watch URL.
        Write complete URL to file.
    '''
    youtube_url = "https://www.youtube.com/watch?v="
    video_ids = []
    with open("response.json", "r") as json_response:
        str_res = json_response.read()
    json_obj = json.loads(str_res)
    for j in json_obj:
        for key, value in j.items():
            if key == "items":
                try:
                    val = value[0]["id"]["videoId"]
                    video_ids.append(val)
                    pass
                except KeyError:
                    pass
                pass
    with open("urls.txt", "w") as urls:
        for v in video_ids:
            url = youtube_url + v
            urls.write("%s\n" % url)
            pass
        print("%d urls written to urls.txt" % len(video_ids))


if __name__ == '__main__':
    #search()
    parseJson()
