# Youtube Search thingie

Extract song names using regex from an [AIMP](http://aimp.ru) playlist file. Use [Google's Youtube API](https://developers.google.com/youtube/v3/) to search for the most relevant song that matches each extracted name as a search query.
Write json response from the API to file, parse this information and exract video ids for the songs found. Construct each song's full Youtube url and write it to a file.